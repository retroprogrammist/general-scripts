#!/usr/bin/env bash
# Description:
# Separated big sql file on tables and put them to tablesFolder
# After run this shell script
echo "start"
docker ps
for filename in ./tablesFolder/*.sql; do
  echo "$filename"
  docker exec -i actility-b-db-1 mysql --user=magento --password=magento magento < "$filename"
done
echo "finish"