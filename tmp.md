
### COMMON

```bash
docker exec -it [DOCKER CONTAINER] mysql -e 'SHOW VARIABLES' --user=magento --password=magento magento > variables.log

docker exec -it [DOCKER CONTAINER] mysql -e 'SET GLOBAL log_bin_trust_function_creators = 1' --user=root --password=root

php -d memory_limit=-1 [COMMAND]
```


### MAGENTO
```bash
bin/magento set:up && bin/magento set:di:comp & bin/magento ind:reind && bin/magento cache:f

php bin/magento admin:user:create --admin-user=rzhanau --admin-password="123qweASD" --admin-email="alexey.rzhanov@onilab.com" --admin-firstname=Alex --admin-lastname=Rzh

bin/magento admin:user:create --admin-user=alexey.rzhanov --admin-password="123qweASD" --admin-email="alexey.rzhanov@onilab.com" --admin-firstname=Alex --admin-lastname=Rzh
```


### DB IMPORT
```bash
pv christofle-20230519.sql.tar.gz | gunzip | mysql -h 127.0.0.1 -P 3306 --user=magento --password=magento magento
```
**or**
```bash
pv christofle-db.sql | mysql -h 127.0.0.1 -P 3306 --user=magento --password=magento magento
```

Чтобы увидеть оригинал svc файла без обработки браузером введи гет параметр WSDL или singleWsdl 
Ex: https://www.test.test/ReceiptExternal.svc?singleWsdl

Самоподписной сертификат https://devopscube.com/create-self-signed-certificates-openssl/