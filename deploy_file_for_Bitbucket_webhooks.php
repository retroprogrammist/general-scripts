<?php
@set_time_limit(0);
$need_key = false;

$home = getenv('HOME');
if(!is_dir($home)) {
    $home = '/home/bitrix/';
}

$dirEx = $_SERVER['DOCUMENT_ROOT'];
if(empty($dirEx) || !is_dir($dirEx)) {
    $dirEx = './';
}

if($need_key) {
    $git = CGit::init()->shell($home, [
        'pwd',
        'mkdir ~/.ssh',
        'cd ~/.ssh',
        'eval "$(ssh-agent -s)"',
        'ssh-keygen -q -t rsa -Т -f id_rsa',
        'ssh-add ~/.ssh/id_rsa',
        'chmod 600 ~/.ssh/id_rsa',
//        'touch ~/.ssh/authorized_keys',
//        'chmod 600 ~/.ssh/authorized_keys',
        'ls ~/.ssh',
        'cat ~/.ssh/id_rsa.pub',
        'ssh git@bitbucket.org',
    ])->getLog();
} else {
    file_put_contents(__DIR__."/log.log", print_r($_SERVER, true), FILE_APPEND);
    $git = CGit::init()->shell($home, [
        'pwd',
        'echo home=$HOME',
        'whoami',
        'git config --local --list',
        'git config --global --list',
        'git remote -v',
        'git checkout #BRANCH#',
        'git status',
        'git reset --hard HEAD',
//        'git add --all',
//        'git commit -m "Changes on production"',
        'git pull origin #BRANCH#',
//        'git checkout --theirs .',
//        'git commit -am "Remote Conflict"',
//        'git push origin master', //or
//        'git merge -s recursive -Xtheirs'
        'git status',
        'git submodule sync',
        'git submodule update',
        'git submodule status',

    ], '/home/bitrix/ext_www/dev.profildoors-usa.com/');
    file_put_contents(__DIR__."/step.log", '1', FILE_APPEND);
    if (filesize(__DIR__ . '/execafterdeploy.php')>0) {

        $git->addLog('', '--- In execafterdeploy.php ---')->shell($home, [
            'cat execafterdeploy.php',
        ]);

        require_once __DIR__ . '/execafterdeploy.php';

        file_put_contents(__DIR__ . '/execafterdeploy.php', '');
        $git->addLog('', '--- Executing execafterdeploy.php ---')->shell($home, [
            'git status',
//            'git add --all',
//            'git commit -m "Changes on production"',
//            'git push origin master',
        ]);
        file_put_contents(__DIR__."/step.log", '2', FILE_APPEND);
    }
    file_put_contents(__DIR__."/step.log", '3', FILE_APPEND);
    file_put_contents(__DIR__."/log.html", $git->getLog(true));
    file_put_contents(__DIR__."/step.log", '4', FILE_APPEND);
}

class CGit {
    public static function init(){return new self;}
    private function __construct(){$this->body = '';}
    public function shell (string $home = '/', array $commands = [], string $dirExecution='./') {
        putenv('HOME='.$home);
        foreach ($commands AS $command) {$this->shell_exec($command, $dirExecution);}
        return $this;
    }
    private function shell_exec(string $command = '', string $dirExecution='./') {
        if (!empty($command)) {
            $out = shell_exec("cd " . $dirExecution . '&&' . $command . " 2>&1 ");
            $this->addLog("{$command}", $out);
        }
    }
    public function addLog($k = '',$v = '') {
        if(!empty($k)) $this->body .= '<div><span style="color: #379c1a;">$</span> <span style="color: #729FCF;">'.$k.'</span></div>';
        if(!empty($v)) $this->body .= '<div style="padding-left:20px;">' . nl2br(htmlentities(trim($v))) . '</div>';
        return $this;
    }
    public function getLog($return = false) {
        $html = '<div style="color: #eeeeee; background-color: #2b2b2b; padding: 10px;">'.$this->body.'</div>';
        if ($return) {return $html;} else {echo $html; return $this;}
    }
}
