<?php
namespace Example\Ajax\Controller;

use \Bitrix\Main\ErrorCollection,
    \Bitrix\Main\Engine\ActionFilter,
    \Bitrix\Main\Engine\Response\AjaxJson;

class ClassName extends \Bitrix\Main\Engine\Controller
{

    public function configureActions() : array
    {
        return [
            'NameOfMethod' => [ //Without "Action" prefixes
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(true, 'your_session_id'),
                ],
            ]
        ];
    }

    public function NameOfMethodAction($param1 = [], $param2 = '') : AjaxJson
    {
        if($error = false) { //ERROR one of the ways
            $eCollection = new ErrorCollection();
            $eCollection->setError(new \Bitrix\Main\Error('Menu file not found'));

            return AjaxJson::createError($eCollection);
        }

        return new AjaxJson(['$param1'=>$param1, '$param2'=>$param2]);
    }
}