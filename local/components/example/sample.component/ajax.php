<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Engine\Controller;

class CComponentClassName extends Controller
{
    public function configureActions() : array
    {
        return [
            'NameOfMethod' => [
                'prefilters' => [ //Это стандартные настройки по умолчанию, можно вообще удалить и будет тоже самое
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ]
        ];
    }

    public static function NameOfMethodAction($param2 = 'qwerty', $param1 = 'admin')
    {
        return 'Nice!';
    }

}