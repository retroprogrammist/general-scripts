<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Engine\ActionFilter,
    \Bitrix\Main\Engine\Contract\Controllerable;

use \Bitrix\Main\Error,
    \Bitrix\Main\ErrorCollection,
    \Bitrix\Main\Engine\Response\AjaxJson;

class CComponentClassName extends CBitrixComponent implements Controllerable
{

    public function configureActions() : array
    {
        return [
            'NameOfMethod' => [
                'prefilters' => [ //Это стандартные настройки по умолчанию, можно вообще удалить и будет тоже самое
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ]
        ];
    }

    public function executeComponent(){}

    public function NameOfMethodAction($param1 = 42, $param2 = true)
    {
        if($param1 === 69) {
            /** Способ вернуть ошибку  */
            $error = new Error('Error Message: ');
            $errorCollection = new ErrorCollection([$error]);
            return AjaxJson::createError( $errorCollection );
        }

        return [ //Можно просто отдать валидные данные для функции json_encode()
            '$param1' => $param1,
            '$param2' => $param2,
        ];


    }
}