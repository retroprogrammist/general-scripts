<?
use Bitrix\Main\Loader;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*DEMO CODE for component inheritance
CBitrixComponent::includeComponentClass("bitrix::news.base");
class CBitrixCatalogSmartFilter extends CBitrixNewsBase
*/
class CBitrixCatalogSmartFilter extends CBitrixComponent
{
	/*
	This function takes an array (arTuple) which is mix of scalar values and arrays
	and return "rectangular" array of arrays.
	For example:
	array(1, array(1, 2), 3, arrays(4, 5))
	will be transformed as
	array(
		array(1, 1, 3, 4),
		array(1, 1, 3, 5),
		array(1, 2, 3, 4),
		array(1, 2, 3, 5),
	)
	*/
    //old function, probably have problem with memory;
    function ArrayMultiply(&$arResult, $arTuple, $arTemp = array())
    {

        file_put_contents(__DIR__."/log.log", 'a');
        if($arTuple)
        {
            reset($arTuple);
            $key = key($arTuple);
            $head = $arTuple[$key];
            unset($arTuple[$key]);
            $arTemp[$key] = false;
            if(is_array($head))
            {
                if(empty($head))
                {
                    if(empty($arTuple))
                        $arResult[] = $arTemp;
                    else
                        $this->ArrayMultiply($arResult, $arTuple, $arTemp);
                }
                else
                {
                    foreach($head as $value)
                    {
                        $arTemp[$key] = $value;
                        if(empty($arTuple))
                            $arResult[] = $arTemp;
                        else
                            $this->ArrayMultiply($arResult, $arTuple, $arTemp);
                    }
                }
            }
            else
            {
                $arTemp[$key] = $head;
                if(empty($arTuple))
                    $arResult[] = $arTemp;
                else
                    $this->ArrayMultiply($arResult, $arTuple, $arTemp);
            }
        }
        else
        {
            $arResult[] = $arTemp;
        }
    }

    function ArrayMultiplyUsingLoops(&$arResult, $arTuple, $arTemp = array())
    {
        $result = [[]];
        reset($arTuple);

        while(is_array($arTuple) && !empty($arTuple)) {
            $arTemp = [];
            $chunk_key = \key($arTuple);
            $chunk = $arTuple[$chunk_key]; //array_shift - we can't use it for numeric keys
            unset($arTuple[$chunk_key]);

            if(\is_array($chunk)&&!empty($chunk)) {
                foreach($chunk as $val) {
                    foreach ($result as $resElement) {
                        if(!empty($val)) {
                            $resElement[$chunk_key] = $val;
                            $arTemp[] = $resElement;
                        }
                    }
                }
                $result = $arTemp;
            }
        }

        /*global $USER;
        if($USER->isAdmin()) {
            file_put_contents(__DIR__.'/log.log', print_r([count($arResult), count($result)], true));
            file_put_contents(__DIR__.'/arRes.log', print_r($l, true));
            file_put_contents(__DIR__.'/res.log', print_r($result, true));
        }*/

        $arResult = array_merge($arResult, $result);
        //$arResult = $result;
    }
}
