/**
 * Скрипты для ajax контроллеров в Битрикс, отправка
 * Смотри /local/modules/example.ajax
 * @todo вопрос 1 - соглашение о наименовании ебанутое мальнько, нужно понять а что если папка называется в n {2,} слов? Или вообще 1 слово?
 * @todo вопрос 2 - как в jQuery или нативном jScript передать параметр навигации navigation?
 */

    /**
     * МОДУЛЬ
     *
     * FOLDER
     * /[local, bitrix]/modules/example.ajax - будет искать модуль по пути либо в local, либо в bitrix папке, в угоду обратной поддержки, используйте local
     *
     * наименование нужного метода example:ajax.ClassName.NameOfMethod:
     * * example:ajax - папка модуля example.ajax - . на : меняй и вся магия
     * * ClassName - имя файла/класса - по PSR-4 один хер, правда такая хуита только с 20 версии ядра Битрикса, до нее там файлы и папки в нижнем регистре быть должны
     * * NameOfMethod - это имя метода БЕЗ Action приставки, нахера так было делать только святой пиксель знает
     *
     * * your_session_id - подставьте свое наименование переменной, по умолчанию в Bitrix это sessid
     *
     * * Если вы юзаете Алиасы свои, то путь был бы таким example:ajax.alias.ClassName.NameOfMethod
     */

        //Используя битриксовый 'jQuery'
        BX.ajax.runAction('example:ajax.ClassName.NameOfMethod', {
            data:{param1: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ', param2: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ'},
            method : 'POST', //разбирайтесь сами в доках, там хватает настроек конфига, если найдете, а лучше в исходники капайте
            navigation : {} //для навигации юзается
        }).then(function(response){console.log(response);}); //{data:{}} ну вот так они решили, что нужно, хоти


        //Тоже самое используя jQuery
        let query = {
            action: 'example:ajax.ClassName.NameOfMethod',
        };

        let data = {
            param1: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ',
            param2: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ',
            SITE_ID: 's1',
            your_session_id: BX.message('bitrix_sessid') || phpVars.bitrix_sessid || BX.bitrix_sessid() //Смотри класс, там название переменной в префильтре установлено, по умолчанию 'sessid'
        };

        let request = $.ajax({
            url: '/bitrix/services/main/ajax.php?' + $.param(query),
            method: 'POST',
            data: data
        });

        request.done(function (response) {
            console.log(response);
        });


        //Тоже самое нативный JS
        let url, response;

        if (window.URL) {
            url = new URL(window.location.protocol + '//' + window.location.host + '/bitrix/services/main/ajax.php');
            url.searchParams.set('action', 'example:ajax.ClassName.NameOfMethod');
        } else {
            url = window.location.protocol + '//' + window.location.host + '/bitrix/services/main/ajax.php?action=example:ajax.ClassName.NameOfMethod';
        }

        request = new FormData();
        request.append('your_session_id', BX.message('bitrix_sessid') || phpVars.bitrix_sessid || BX.bitrix_sessid());
        request.append('param1', 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ'); // Для объектов используйте JSON.toString()
        request.append('param2', 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ'); // Для объектов используйте JSON.toString()

        let xhr = new XMLHttpRequest(); //Используя XMLHttpRequest

        xhr.open('POST', url);
        xhr.send(request);

        xhr.onload = function() {//response
            console.log(`Загружено: ${xhr.status} ${xhr.response}`);
        };

        xhr.onerror = function() { // происходит, только когда запрос совсем не получилось выполнить
            console.log(`Ошибка соединения`);
        };


        response = await fetch(url, {//Или через fetch
            method: 'POST',
            body: request
        });

        console.log(await response.json());

    /**
     * Компонент
     *
     * FOLDER
     * /[local, bitrix]/components/example/sample.component
     *
     * наименование нужного метода example:sample.component и отдельно имя действия (метода) NameOfMethod:
     * * example - папка c пространством компонентов
     * * sample.component - сам компонент
     * * NameOfMethod - это имя метода БЕЗ Action приставки
     *
     * Класс не важен, тк берется из файла ajax.php или class.php ПОСЛЕДНИЙ в файле класс унаследованный
     * от Bitrix\Main\Engine\Controller или \Bitrix\Main\Engine\Contract\Controllerable
     */

        //Используя битриксовый 'jQuery'
        request = BX.ajax.runComponentAction('example:sample.component', 'NameOfMethod', {
            mode:'class', //'ajax' - это из какого файла брать будет, ajax.php или class.php
            data: {
                param1: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ',
                param2: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ'
            }
        });

        request.then(function(response){
            console.log(response);
        });


        //Тоже самое используя jQuery
        query = {
            c: 'example:sample.component',
            action: 'NameOfMethod',
            mode: 'class' //'ajax' - это из какого файла брать будет, ajax.php или class.php
        };

        data = {
            param1: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ',
            param2: 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ',
            SITE_ID: 's1', //это как хотите, для многосайтовости
            your_session_id: BX.message('bitrix_sessid') || phpVars.bitrix_sessid || BX.bitrix_sessid()
        };

        request = $.ajax({
            url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
            method: 'POST',
            data: data
        });

        request.done(function (response) {
            console.log(response);
        });


        //Тоже самое нативный JS
        if (window.URL) {
            url = new URL(window.location.protocol + '//' + window.location.host + '/bitrix/services/main/ajax.php');
            url.searchParams.set('c', 'example:sample.component');
            url.searchParams.set('action', 'NameOfMethod');
            url.searchParams.set('mode', 'class'); //'ajax' - это из какого файла брать будет, ajax.php или class.php
        } else {
            url = window.location.protocol + '//' + window.location.host + '/bitrix/services/main/ajax.php?c=example:sample.component&action=NameOfMethod&mode=class';
        }

        request = new FormData();
        request.append('your_session_id', BX.message('bitrix_sessid') || phpVars.bitrix_sessid || BX.bitrix_sessid());
        request.append('param1', 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ');
        request.append('param2', 'ТУТ ВАШИ ПОЛЕЗНЫЕ ДАННЫЕ');

        xhr = new XMLHttpRequest(); //Используя XMLHttpRequest

        xhr.open('POST', url);
        xhr.send(request);

        xhr.onload = function() {//response
            console.log(`Загружено: ${xhr.status} ${xhr.response}`);
        };

        xhr.onerror = function() { // происходит, только когда запрос совсем не получилось выполнить
            console.log(`Ошибка соединения`);
        };


        response = await fetch(url, {//Или через fetch
            method: 'POST',
            body: request
        });

        console.log(await response.json());