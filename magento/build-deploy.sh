#!/bin/bash
export HTTPS=on
./bin/magento maintenance:enable
./bin/magento cache:clean
rm -fR pub/static/adminhtml
rm -fR pub/static/frontend
rm -f pub/static/deployed_version.txt
rm -fR var/cache
rm -fR var/composer_home
rm -fR var/generation
rm -fR var/page_cache
rm -fR var/view_preprocessed
rm -fR var/view_preprocessed
./bin/magento setup:upgrade
php -d memory_limit=-1 bin/magento setup:di:compile
php -d memory_limit=-1 bin/magento setup:static-content:deploy -f
./bin/magento setup:static-content:deploy -f
./bin/magento cache:flush
./bin/magento maintenance:disable